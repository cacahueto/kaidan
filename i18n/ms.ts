<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Klien Jabber/XMPP yang mudah, mesra pengguna</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Lesen:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Lihat kod sumber dalam talian</translation>
    </message>
</context>
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">Latar Belakang</translation>
    </message>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation type="vanished">Klien Jabber/XMPP yang mudah, mesra pengguna</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Lesen:</translation>
    </message>
    <message>
        <source>Sourcecode on GitHub</source>
        <translation type="vanished">Kod sumber di GitHub</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Tutup</translation>
    </message>
</context>
<context>
    <name>AboutSheet</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation type="obsolete">Klien Jabber/XMPP yang mudah, mesra pengguna</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="obsolete">Lesen:</translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <source>Navigate Back</source>
        <translation type="vanished">Ke Belakang</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Tukar kata laluan</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Kata laluan semasa:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Kata laluan baru:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Kata laluan baru (ulang semula):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Kata laluan baru yang diulang tidak sepadan.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>Kata laluan semasa tidak sah.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Anda mesti sambung ke internet untuk menukar kata laluan anda.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Selepas anda menukar kata laluan, anda perlu log masuk semula di kesemua peranti anda yang lain.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Tukar</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Message copied to clipboard</source>
        <translation type="vanished">Mesej disalin ke papan klip</translation>
    </message>
    <message>
        <source>Copy Message</source>
        <translation>Salin Mesej</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>Sunting Mesej</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Muat Turun</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Tulis mesej</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Hantar</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Imej</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Dokumen</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>Fail lain</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Pilih fail</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Hantar mesej spoiler</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>Tip spoiler</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContextDrawer</name>
    <message>
        <source>Actions</source>
        <translation type="vanished">Tindakan</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Tidak dapat menyimpan fail: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Muat turun gagal: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation>Kegemaran</translation>
    </message>
    <message>
        <source>People</source>
        <translation>Manusia</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation>Alam</translation>
    </message>
    <message>
        <source>Food</source>
        <translation>Makanan</translation>
    </message>
    <message>
        <source>Activity</source>
        <translation>Aktiviti</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Perlancongan</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation>Objek</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>Simbol</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation>Bendera</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cari</translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation>Cari emoji</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Sila pilih seseorang untuk mula menghantar mesej</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>Pilih fail</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Close</source>
        <translation>Tutup</translation>
    </message>
    <message>
        <source>Go to parent folder</source>
        <translation>Naik ke folder atas</translation>
    </message>
</context>
<context>
    <name>ForwardButton</name>
    <message>
        <source>Navigate Forward</source>
        <translation type="vanished">Ke Depan</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation>Log keluar</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Latar Belakang</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation type="vanished">Tambah kenalan baru</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Kembali</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Undang kawan</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Pautan undangan telah disalin ke papan klip</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Tetapan</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="vanished">Tidak dapat hantar mesej, kerana anda tiada sambungan internet.</translation>
    </message>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation type="vanished">Tidak dapat menambah kenalan, kerana anda tiada sambungan internet.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation type="vanished">Tidak dapat membuang kenalan, kerana anda tiada sambungan internet.</translation>
    </message>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Pautan akan dibuka selepas anda disambungkan.</translation>
    </message>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation type="vanished">Tidak dapat menghantar fail, kerana anda tidak disambung ke internet.</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>Jabber-ID anda：</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>contoh＠emel.com</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>Kata Laluan Anda：</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Kata Laluan</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Cuba Lagi</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Sambung</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Menyambung…</translation>
    </message>
    <message>
        <source>Log in</source>
        <translation>Log masuk</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Log masuk akaun XMPP anda</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">ID diaspora* anda:</translation>
    </message>
    <message>
        <source>user@diaspora.pod</source>
        <translation type="vanished">nama@diaspora.pod</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Nama pengguna atau kata laluan tidak sah.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Tidak dapat menghubungi pelayan. Sila periksa sambungan internet anda.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>Pelayan tidak menyokong sambungan selamat.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Ralat ketika cuba untuk menyambung secara selamat.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation>Tidak dapat menetapkan alamat pelayan. Sila periksa semula JID anda.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>Tidak dapat menghubungi pelayan.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Protokol pengesahan tidak disokong oleh pelayan.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>Telah berlaku ralat tidak diketahui; lihat log untuk maklumat lanjut.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>Tidak dapat hantar mesej, kerana anda tiada sambungan internet.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>Tidak dapat membetulkan mesej, kerana anda tidak sambung ke internet.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Mesej gagal dihantar.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>Pembetulan mesej tidak berjaya.</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">Sedang lapang</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">Boleh sembang</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">Tidak lapang</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">Jangan ganggu</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">Tidak lapang lebih lama</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Ralat</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation>Kata laluan berjaya ditukar.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Gagal menukar kata laluan: %1</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Nickname:</source>
        <translation>Nama panggilan:</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>ID Jabber:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>contoh＠emel.com</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Tambah</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Tambah kenalan baru</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Ini juga akan menghantar permintaan untuk mencapai kehadiran kenalan.</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Mesej pilihan:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Beritahu pasangan sembang siapakah diri anda sebenarnya.</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Ralat: Sila periksa JID.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Sedang lapang</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Boleh sembang</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Tidak lapang</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Jangan ganggu</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Tidak lapang lebih lama</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">Tiada dalam talian</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Ralat</translation>
    </message>
    <message>
        <source>Invalid</source>
        <translation type="vanished">Tidak sah</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Tidak dapat menambah kenalan, kerana anda tiada sambungan internet.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Tidak dapat membuang kenalan, kerana anda tiada sambungan internet.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Contacts</source>
        <translation>Kenalan</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Tambah kenalan baru</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Menyambung…</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Luar talian</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &quot;%1&quot; from your roster?</source>
        <translation type="vanished">Adakah anda ingin memadam kenalan &quot;%1&quot; daripada roster anda?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Padam kenalan</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Padam</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Adakah anda ingin memadam kenalan &lt;b&gt;%1&lt;/b&gt; daripada roster anda?</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Batal</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Hantar</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation>Kapsyen</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Tetapan</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>Tukar kata laluan</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Tukar kata laluan akaun anda. Anda perlu log masuk semula di kesemua peranti anda yang lain.</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Permintaan Langganan</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Anda menerima permintaan langganan daripada &lt;b&gt;%1&lt;/b&gt;. Jika anda terima, akaun tersebut boleh mencapai status kehadiran anda.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Tolak</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Terima</translation>
    </message>
</context>
<context>
    <name>ToolBarApplicationHeader</name>
    <message>
        <source>More Actions</source>
        <translation type="vanished">Tindakan Lanjut</translation>
    </message>
</context>
<context>
    <name>ToolBarPageHeader</name>
    <message>
        <source>More Actions</source>
        <translation type="obsolete">Tindakan Lanjut</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Tidak dapat menghantar fail, kerana anda tidak disambung ke internet.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Fail</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Latar Belakang</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
