<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Un client Jabber/XMPP semplice e intuitivo</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licenza:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Visualizza il codice sorgente online</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Cambia password</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Password attuale:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Nuova password:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Nuova password (ripeti):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Le nuove password non corrispondono.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>La password attuale non è corretta.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Devi essere connesso per cambiare la tua password.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Dopo aver cambiato la tua password dovrai reinserirla in tutti i tuoi altri dispositivi.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Copy Message</source>
        <translation>Copia Messaggio</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>Modifica Messaggio</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Componi il messaggio</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Documento</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>Altro file</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Seleziona un file</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Invia un messaggio con spoiler</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>Accenno dello spoiler</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Impossibile salvare il file: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Scaricamento non riuscito: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation>Preferiti</translation>
    </message>
    <message>
        <source>People</source>
        <translation>Persone</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation>Natura</translation>
    </message>
    <message>
        <source>Food</source>
        <translation>Cibo</translation>
    </message>
    <message>
        <source>Activity</source>
        <translation>Attività</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Viaggi</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation>Oggetti</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>Simboli</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation>Bandiere</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation>Cerca emoji</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Seleziona una conversazione per iniziare a messaggiare</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>Seleziona un file</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Go to parent folder</source>
        <translation>Vai al livello superiore</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation>Esci</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Più Informazioni</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Invita amici</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Collegamento dell&apos;invito copiato negli appunti</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Il collegamento verrà aperto appena ti sarai connesso.</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Accedi</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Accedi al tuo account XMPP</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>Il tuo Jabber-ID:</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">Il tuo diaspora*-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>nome@esempio.org</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>La tua password:</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Connessione…</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Connetti</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Nome utente o password non validi.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Impossibile connettersi al server. Per favore controlla la tua connessione a internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>Il server non supporta connessioni sicure.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Errore durante il tentativo di connessione sicura.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation>Impossibile risolvere l&apos;indirizzo del server. Per favore, controlla di nuovo il tuo JID.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>Non posso connettermi al server.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Protocollo di autenticazione non supportato dal server.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>Si è verificato un errore sconosciuto; per i dettagli guarda il registro.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>Impossibile inviare messaggi, perchè non sei connesso.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>Impossibile correggere messaggi, perchè non sei connesso.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Il messaggio non può essere inviato.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>La correzione del messaggio non è riuscita.</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">Disponibile</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">Disponibile a conversare</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">Lontano dal dispositivo</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">Non disturbare</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">Lontano per tanto tempo</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Non in linea</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Errore</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation>La password è stata cambiata correttamente.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Cambio password non riuscito: %1</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Aggiungi nuovo contatto</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>nome@esempio.org</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Questo invierà anche una richiesta di accesso allo stato di presenza del contatto.</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-ID:</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Soprannome:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Messaggio opzionale:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Scrivi al contatto della conversazione chi sei.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Offline</source>
        <translation type="vanished">Non in linea</translation>
    </message>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Errore: per favore controlla il JID.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Disponibile</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Disponibile a conversare</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Lontano dal dispositivo</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Non disturbare</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Lontano per tanto tempo</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Errore</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Non è possibile aggiungere il contatto, perchè non sei connesso.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Impossibile rimuovere il contatto, perchè non sei connesso.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Connessione…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Contatti</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Aggiungi nuovo contatto</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Non in linea</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Vuoi veramente eliminare il contatto &lt;b&gt;%1&lt;/b&gt; dalla tua lista?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Elimina contatto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Annulla</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Invia</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>Cambia password</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Cambia la password del tuo account. Dovrai reinserirla negli altri tuoi dispositivi.</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Richiesta di Sottoscrizione</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Hai ricevuto una richiesta di sottoscrizione da &lt;b&gt;%1&lt;/b&gt;. Se la accetti, l&apos;account avrà accesso al tuo stato di presenza.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Rifiuta</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Accetta</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Impossibile inviare file, perchè non sei connesso.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Più Informazioni</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
